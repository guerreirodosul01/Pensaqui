<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/controle', 'ProfessorController@index')->name('controle');
Route::get('/principal', 'ProfessorController@principal')->name('principal');
Route::get('/aluno', 'ProfessorController@aluno')->name('aluno');
Route::get('/escola', 'ProfessorController@escola')->name('escola');
Route::get('/professor', 'ProfessorController@professor')->name('professor');
Route::get('/turma', 'ProfessorController@principal')->name('turma');
Route::get('/usuarios', 'ProfessorController@usuarios')->name('usuarios');
Route::get('/logout', 'ProfessorController@logout')->name('logout');
