tinymce.init({
    selector: "textarea",
    plugins: 'link image code',
	style_formats: [
        {title: 'Branco', inline: 'span', styles: {color: '#FFF'}},
        {title: 'Laranja', inline: 'span', styles: {color: '#d2823b'}}
    ],
	setup: function (editor) {editor.on('SaveContent', function (e) {e.content = e.content.replace("'", "&apos;");});}
});