<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
Design by Free CSS Templates
http://www.freecsstemplates.org
Released for free under a Creative Commons Attribution 2.5 License

Name       : Spotless 
Description: A two-column, fixed-width design with dark color scheme.
Version    : 1.0
Released   : 20130921

-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title></title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet" />
<link href="default.css" rel="stylesheet" type="text/css" media="all" />
<link href="fonts.css" rel="stylesheet" type="text/css" media="all" />

<!--[if IE 6]><link href="default_ie6.css" rel="stylesheet" type="text/css" /><![endif]-->

</head>
<body>
<div id="header" class="container">
	<div id="logo">
		<h1><a href="#">PensaQui</a>&nbsp;&nbsp;&nbsp;<img border="0" width="178" height="73" src="images/chemical.jpg" /></h1>
		<span>Um Objeto Educacional sobre transforma��es qu�micas</span>
		
	</div>
</div>
<div id="menu-wrapper">
	<div id="menu" class="container">
		<ul>
			<li><a href="?o=inicial" accesskey="1" title="">Inicial</a></li>
			<li><a href="?o=sobre_projeto" accesskey="2" title="">Sobre o Projeto</a></li>
			<li><a href="?o=como_aplicar" accesskey="3" title="">Como aplicar</a></li>
			<li><a href="?o=relatorio_aplicacao" accesskey="4" title="">Relat�rio de aplica��o</a></li>
			<li><a href="?o=contato" accesskey="5" title="">Contato</a></li>
		</ul>
	</div>
</div>

<!--
<div id="banner" class="container">
	<img src="images/banner.jpg" alt="" class="image image-full" />
</div>
-->


<?php

if (empty($_GET["o"]) OR $_GET["o"] == "inicial")
{	
	?>
	<div id="page" class="container">
		
		<div class="tbox1">
			<!--<h3>Welcome to our website</h3>-->
			<p style="text-align:justify;">O <strong>PensaQui</strong> � um Objeto Educacional que busca por meio de novas tecnologias, aproximar estudantes e conte�dos da qu�mica. 
			Tem como objetivos aprimorar os processos de ensino e aprendizagem.</p>
			
			<p style="text-align:justify;">
			A aplica��o do PensaQui possibilita conhecer como os estudantes explicam as situa��es envolvendo transforma��es qu�micas vincenciadas no seu dia a dia, submetendo 
			o estudante � an�lise das transforma��es qu�micas em diferentes n�veis de representa��o (macrosc�pico, submicrosc�pico e simb�lico).
			</p>
			
			<p style="text-align:justify;">
			O estudante que interage com o objeto � convidado a manifestar suas opini�es a respeito dos fatos e todo o caminho percorrido por ele, suas escolhas 
			e seus textos constru�dos em diferentes momentos, far�o parte de um relat�rio que pode acessado pelo professor ap�s a aplica��o do PensaQui.
			</p>
			
			<!--<a href="#" class="button">Etiam posuere</a>-->
		</div>
		<div class="tbox2">
			<img src="images/image_pensaqui.jpg" alt="" class="image image-full" />
		</div>
	</div>
	<?
}

if ($_GET["o"] == "sobre_projeto")
{
	?>
	<div id="page" class="container">
		
		<div class="header">
			<h2>SOBRE O PROJETO</h2>
			<span>Saiba mais sobre o projeto PensaQUI</span>
		</div>
		
		<div class="tbox1_long">
			<p>Sem conte�do ainda</p>		
			
		</div>
		
	</div>
	<?
}

if ($_GET["o"] == "como_aplicar")
{
	?>
	<div id="page" class="container">
		
		<div class="header">
			<h2>COMO APLICAR</h2>
			<span>Veja os passos para a aplica��o do PensaQUI</span>
		</div>
		
		<div class="tbox1_long">
			<p><img src="images/chemical.png"><br /><b>Passo 1</b> - Realizar o cadastro inicial clicando <strong><a href="#">AQUI</a></strong> (deve ser feito com anteced�ncia � aplica��o);</p>		
			
			<br />
			<p><img src="images/chemical.png"><img src="images/chemical.png"><br /><b>Passo 2</b> - Aplicar o objeto para os estudantes. Clique <strong><a href="#">AQUI</a></strong> para baixar o Guia do Professor;</p>		
			
			<br />
			<p><img src="images/chemical.png"><img src="images/chemical.png"><img src="images/chemical.png"><br /><b>Passo 3</b> - Acessar o Relat�rio de Aplica��o, clicando <strong><a href="#">AQUI</a></strong>.</p>
			
		</div>
		
	</div>
	<?
}


if ($_GET["o"] == "relatorio_aplicacao")
{
	?>
	<div id="page" class="container">
		
		<div class="header">
			<h2>RELAT�RIO DE APLICA��O</h2>
			<span>Veja os resultados da aplica��o do PensaQUI</span>
		</div>
		
		<div class="tbox1_long">
			<p>Aqui o professor atrav�s de seu CPF e senha, poder� buscar as respostas dadas por seus alunos, agrupados por intervalo de tempo (caso apliquem a mais de uma turma)</p>		
			<p>Como teremos os dados de todos as aplica��es dos estudantes, podemos no cadastro do professor j� ter uma caixinha em que ele concordar� com o compartilhamento dos dados an�nimos de seus alunos.</p>
			
		</div>
		
	</div>
	<?
}


if ($_GET["o"] == "contato")
{
	?>
	<div id="page" class="container">
		
		<div class="header">
			<h2>CONTATO</h2>
			<span>Entre em contato com a equipe idealizadora do PensaQUI</span>
		</div>
		
		<div class="tbox1_long">
			<p>Aqui ir� ter um formul�rio de contato para o p�blico.</p>		
			
			
		</div>
		
	</div>
	<?
}

?>

</body>
</html>
