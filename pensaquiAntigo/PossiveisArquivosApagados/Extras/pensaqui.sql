-- --------------------------------------------------------

--
-- Estrutura da tabela `pensaqui_aluno`
--

CREATE TABLE IF NOT EXISTS `pensaqui_aluno` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `turma` text NOT NULL,
  `professor` text NOT NULL,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `senha` varchar(255) NOT NULL DEFAULT '',
  `email` text NOT NULL,
  `respostaa` text NOT NULL,
  `respostab` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;


--
-- Estrutura da tabela `pensaqui_escola`
--

CREATE TABLE IF NOT EXISTS `pensaqui_escola` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;


--
-- Estrutura da tabela `pensaqui_professor`
--

CREATE TABLE IF NOT EXISTS `pensaqui_professor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `escola` int(11) NOT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  `login` varchar(30) NOT NULL DEFAULT '',
  `senha` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Estrutura da tabela `pensaqui_turma`
--

CREATE TABLE IF NOT EXISTS `pensaqui_turma` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` text NOT NULL,
  `escola` text NOT NULL,
  `professor` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;


--
-- Estrutura da tabela `pensaqui_usuarios`
--

CREATE TABLE IF NOT EXISTS `pensaqui_usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `login` varchar(150) NOT NULL DEFAULT '',
  `senha` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Extraindo dados da tabela `pensaqui_usuarios`
--

INSERT INTO `pensaqui_usuarios` (`id`, `nome`, `login`, `senha`) VALUES
(1, 'Administrador', 'admin', 'YWRtaW4=');
