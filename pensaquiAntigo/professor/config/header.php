<title>PAINEL DE CONTROLE</title>
<meta charset="utf-8">

<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700"/>
<link rel="stylesheet" type="text/css" href="../controle/scripts/css.css" >

<link href="../controle/scripts/jquery.modal.css" type="text/css" rel="stylesheet" />
<link href="../controle/scripts/jquery.modal.theme-xenon.css" type="text/css" rel="stylesheet" />
<link href="../controle/scripts/jquery.modal.theme-atlant.css" type="text/css" rel="stylesheet" />
	
<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script type="text/javascript" src="../controle/scripts/jquery.modal.min.js"></script>
<script type="text/javascript" src="../controle/scripts/painel.js"></script>

