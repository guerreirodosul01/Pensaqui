<?php
require('fpdf.php');

class PDF extends FPDF {	
function Footer() {
    $this->SetY(-15);
    $this->SetFont('Arial','I',8);
    $this->Cell(0,10,$this->PageNo().'/{nb}',0,0,'C');
}
}

$pdf=new PDF();
$header=array('AlunoID:','Nome:','E-mail:','Escolha Panela:','Resposta(s) Panela:', 'Escolha Comprimido:','Resposta(s) Comprimido:');

$table = "pensaqui_aluno";
$turma = $_GET['id'];

include('config/conexao.php');

$sqlturma = mysql_query("SELECT * FROM pensaqui_turma WHERE id LIKE ".$turma."");
while ($rowturma = mysql_fetch_array($sqlturma)){
	$minhaturma = "TURMA: ".utf8_decode($rowturma["nome"]);
	$escola=$rowturma["escola"];
	$professor=$rowturma["professor"];
}

$sqlprof = mysql_query("SELECT * FROM pensaqui_professor WHERE id LIKE ".$professor."");
while ($rowprof = mysql_fetch_array($sqlprof)){
	$minhaprof = "PROFESSOR: ".utf8_decode($rowprof["nome"]);
}
$sqlescola = mysql_query("SELECT * FROM pensaqui_escola WHERE id LIKE ".$escola."");
while ($rowescola = mysql_fetch_array($sqlescola)){
	$minhaescola  = "ESCOLA: ".$rowescola["nome"];
}
   


$pdf->AliasNbPages();

$pdf->SetFont('Arial','',10);
$pdf->AddPage();
$pdf->Image('logo.png',10,10,30);

$pdf->Cell(0,5,$minhaescola,0,2,'R');
$pdf->Cell(0,5,$minhaturma,0,2,'R');
$pdf->Cell(0,5,$minhaprof,0,2,'R');

$pdf->Ln(30);

		
$objQuery = mysql_query("SELECT * FROM ".$table." WHERE turma LIKE ".$turma."");
while ($result = mysql_fetch_assoc($objQuery)){
	$pdf->SetTextColor(0);
	$pdf->SetFillColor(0);

	$pdf->SetFont('','B');
	$pdf->Cell(20,5,$header[0]);
	$pdf->SetFont('');
	$pdf->Cell(80,5,$result['id']);
	$pdf->Ln();
		
	$pdf->SetFont('','B');
	$pdf->Cell(20,5,$header[1]);
	$pdf->SetFont('');
	$pdf->Cell(80,5,utf8_decode($result['nome']));
	$pdf->Ln();
		
	$pdf->SetFont('','B');
	$pdf->Cell(20,5,$header[2]);
	$pdf->SetFont('');
	$pdf->Cell(80,5,$result['email']);
	$pdf->Ln(10);
	
	
	$pdf->SetFont('','B');
	$pdf->Cell(0,5,$header[3]);
	$pdf->Ln(3);	
	$sqlrespostaA = mysql_query("SELECT * FROM pensaqui_resposta WHERE aluno LIKE '".$result['id']."' AND tipo LIKE '3' ORDER BY id DESC LIMIT 1");
	while ($rowrespostaA = mysql_fetch_assoc($sqlrespostaA)){
		$pdf->Ln();
		$pdf->SetTextColor(0);
		$pdf->SetFillColor(0);			
		$pdf->SetFont('');
		$pdf->MultiCell(0,5,$rowrespostaA['txt']);
		$pdf->Ln();
	}	
	$pdf->Ln(5);
	
	$pdf->SetFont('','B');
	$pdf->Cell(0,5,$header[4]);
	$pdf->Ln(3);	
	$sqlrespostaA = mysql_query("SELECT * FROM pensaqui_resposta WHERE aluno LIKE '".$result['id']."' AND tipo LIKE '1' ORDER BY id DESC");
	while ($rowrespostaA = mysql_fetch_assoc($sqlrespostaA)){
		$pdf->Ln();
		$pdf->SetTextColor(0);
		$pdf->SetFillColor(0);			
		$pdf->SetFont('');
		$pdf->MultiCell(0,5,$rowrespostaA['txt']);
		$pdf->Ln();
	}	
	$pdf->Ln(5);
	
	
	$pdf->SetFont('','B');
	$pdf->Cell(0,5,$header[5]);
	$pdf->Ln(3);	
	$sqlrespostaA = mysql_query("SELECT * FROM pensaqui_resposta WHERE aluno LIKE '".$result['id']."' AND tipo LIKE '4' ORDER BY id DESC LIMIT 1");
	while ($rowrespostaA = mysql_fetch_assoc($sqlrespostaA)){
		$pdf->Ln();
		$pdf->SetTextColor(0);
		$pdf->SetFillColor(0);			
		$pdf->SetFont('');
		$pdf->MultiCell(0,5,$rowrespostaA['txt']);
		$pdf->Ln();
	}	
	$pdf->Ln(5);
	
	$pdf->SetFont('','B');
	$pdf->Cell(0,5,$header[6]);
	$pdf->Ln(3);	
	$sqlrespostaB = mysql_query("SELECT * FROM pensaqui_resposta WHERE aluno LIKE '".$result['id']."' AND tipo LIKE '2' ORDER BY id DESC");
	while ($rowrespostaB = mysql_fetch_assoc($sqlrespostaB)){
		$pdf->Ln();
		$pdf->SetTextColor(0);
		$pdf->SetFillColor(0);			
		$pdf->SetFont('');
		$pdf->MultiCell(0,5,$rowrespostaB['txt']);
		$pdf->Ln();
	}	
	$pdf->Ln(10);
}

$hoje=date("Y-m-d-H-i");        

$pdf->Output("pensaqui_relatorio_turma_".$hoje.".pdf","D");
	
	
?>