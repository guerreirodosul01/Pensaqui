<?php include("controle/config/conexao.php"); ?>
<!DOCTYPE html>
<html>
<head>
<title>CADASTRO DE ALUNO</title>
<meta charset="utf-8">

<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700"/>
<link rel="stylesheet" type="text/css" href="controle/scripts/css.css" >
	
<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script type="text/javascript" src="controle/scripts/painel.js"></script>
</head>

<body>
<div id="content">
<div id="nav">
<img src="controle/logo.png"/>
<p>Preencha seus dados ao lado.</p>
</div>


<div class="section">
<?php
if (isset($_GET['opcao'])) {
	$opcao = mysql_real_escape_string($_GET['opcao']);
	$escola = $_POST['escola'];
} else {$opcao = "";}

switch ($opcao) {
//EDITAR
case 'n':?>

<h2>CADASTRO ALUNO</h2>
<form name="cadastro_aluno" id="cadastro_aluno" action="assets/cadastrar_aluno.php" method='post'>
<p><b>Nome</b></p>
<input class="campos2" name="nome" id="nome"  type="text">
<br>
<p><b>Turma</b></p>
<select name="turma" id="turma">
	<option value="0">Seleciona sua turma</option>
    <?php
	mysql_query('SET NAMES utf8');
	$rsturma = mysql_query("SELECT * FROM pensaqui_turma WHERE escola LIKE $escola ORDER BY nome ASC");
	while ($campoturma = mysql_fetch_array($rsturma)){?>    
    <option value="<?= $campoturma["id"] ?>"><?= $campoturma["nome"] ?></option>
	<?php } ?>
</select>
<br>

<p><b>Professor</b></p>
<select name="professor" id="professor">
	<option value="0">Seleciona o(a) professor(a)</option>
    <?php
	$rsprof = mysql_query("SELECT * FROM pensaqui_professor WHERE escola LIKE $escola ORDER BY nome ASC");
	while ($campoprof = mysql_fetch_array($rsprof)){?>    
    <option value="<?= $campoprof["id"] ?>"><?= $campoprof["nome"] ?></option>
	<?php } ?>
</select>
<br>         
<p><b>E-mail</b></p>
<input class="campos2" name="email" id="email"  type="text">
<br>
<p><b>Senha</b> (no mínimo 6 caracteres)</p>
<input class="campos2" type="password" name="senha" id="senha">
<br>  

<input name="respostaa" value="" type="hidden">
<input name="respostab" value="" type="hidden">

<input class="buttoninterno" type=submit name=submit value="CADASTRAR">

</form>
<script>
	$("#nome").focus();
	$("#turma").val(0);
	$("#professor").val(0);
	
	$("#voltar").click(function(){
		
		window.location = 'cadastro.php';
	});
	
	$("#cadastro_aluno").submit(function(){
		if ($("#nome").val().length == 0)
		{
			alert("O nome completo deve ser preenchido.");
			$("#nome").focus();
			return false;
		}
		if ($("#turma").val() == 0)
		{
			alert("O campo turma deve ser preenchido. Caso não disponível, converse com o(a) professor(a).");
			return false;
		}
		if ($("#professor").val() == 0)
		{
			alert("O campo professor(a) deve ser preenchido. Caso não disponível, converse com o(a) professor(a).");
			return false;
		}
		if ($("#email").val().length == 0)
		{
			alert("O e-mail deve ser preenchido.");
			return false;
		}
		if ($("#senha").val().length < 6)
		{
			alert("Uma senha deve ser criada. Deve ter pelo menos 6 caracteres.");
			return false;
		}
	});
</script>

 <?php 
break;

case 'a':?>

<h2>SELECIONE A ESCOLA</h2>
<form name="Fcadastro" id="Fcadastro" action="cadastro.php?opcao=n" method="post">
<select name="escola" id="escola">
	<option value="0">Seleciona sua escola</option>
    <?php
	mysql_query('SET NAMES utf8');
	$rsturma = mysql_query("SELECT * FROM pensaqui_escola ORDER BY nome ASC");
	while ($campoturma = mysql_fetch_array($rsturma)){?>    
    <option value="<?= $campoturma["id"] ?>"><?= $campoturma["nome"] ?></option>
	<?php } ?>
</select>
<br>

<input class="buttoninterno" type=submit name=submit value="AVANÇAR">
</form>

<script>
	$("#escola").val(0);
	
	$("#Fcadastro").submit(function(){
		
		if ($("#escola").val() == 0)
		{
			alert("A escola precisa ser preenchida para continuação.");
			return false;
		}
	});
</script>

 <?php 
break;

case 'p':?>

<h2>CADASTRO PROFESSOR</h2>
<form id='Fprofessor' action="assets/cadastrar_professor.php" method='post'>
<p><b>Nome</b></p>
<input class="campos2" name="nome" id="nome"  type="text">
<br>
<p><b>E-mail</b></p>
<input class="campos2" name="email" id="email"  type="text">
<br>
<!--
<p>Login</p>
<input class="campos2" type="text" name="login" id="login">
<br>
-->
<p><b>Senha</b> (no mínimo 6 caracteres)</p>
<input class="campos2" type="password" name="senha" id="senha">
<br>
<input class="buttoninterno" type=submit name=submit value="SALVAR">
  </form>

<script>
 $("#Fprofessor").submit(function(){
		
		if ($("#nome").val().length == 0)
		{
			alert("O nome completo precisa ser preenchido para continuação.");
			return false;
		}
		if ($("#email").val().length == 0)
		{
			alert("O e-mail precisa ser preenchido para continuação.");
			return false;
		}
		
		if ($("#senha").val().length < 6)
		{
			alert("A senha precisa ser preenchido para continuação e deve possuir pelo menos 6 caracteres.");
			return false;
		}
	
	});
</script>
  <?php 
break;


//PADRAO
default:?>
<h2>VOCÊ É UM..</h2>
<input class="buttoninterno2" type=button onClick="location.href='cadastro.php?opcao=p'" style="width: 30%;float: left;margin: 20px 20px 20px 170px;" value='PROFESSOR'>
<input class="buttoninterno2" type=button onClick="location.href='cadastro.php?opcao=a'" style="width: 30%;float: left;margin: 20px 20px 20px 0px;" value='ALUNO'>
<br>

<?php } ?>


</div>
<div class="break"></div>
<div id="footer"></div>
</div>
</div>

</body>
</html>
