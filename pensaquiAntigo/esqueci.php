﻿<?php include("controle/config/conexao.php"); 

if ($_SERVER['REQUEST_METHOD'] == "POST")
{
	$categoria 	= addslashes($_POST["categoria"]);
	$email 		= addslashes($_POST["email"]);
	
	if ($categoria == "aluno")
	{
		$Q = mysql_query("SELECT id, email, nome FROM pensaqui_aluno WHERE email = '$email'");
		
		if (mysql_num_rows($Q) > 0)
		{
			$A = mysql_fetch_array($Q);
			require("envia_email.php");
			
			$nova_senha = substr(strtoupper(md5(date("dmYHis"))), 0, 6);
			
			$nova_senha64 = base64_encode($nova_senha);
			
			$U = mysql_query("UPDATE pensaqui_aluno SET senha = '$nova_senha64' WHERE id = $A[id]");
			
			if ($U)
			{
				$texto = "Olá, $A[nome].<br /><br />Você solicitou uma nova senha para utilização do PensaQui. Sua nova senha é <b>$nova_senha</b>.<br /><br />PensaQui";
				$assunto = "Recuperação de senha";
				$destinatario = $email;
				
				$resultado = enviaEmail($texto, $destinatario, $assunto);
				
				if ($resultado)
					echo "<script>alert('Um e-mail foi enviado para o endereço cadastrado contendo uma nova senha.'); window.location = 'index.html';</script>";
				else
					echo "<script>alert('Ocorreu um erro no envio do e-mail de recuperação senha. Tente refazer o processo.'); window.location = 'esqueci.php';</script>";
			}
			else
			{
				echo "<script>alert('Ocorreu um problema para alteração da senha. Tente refazer o processo.'); window.location = 'esqueci.php';</script>";
			}
			
			exit;
		}
		else
		{
			echo "<script>alert('Este e-mail não foi encontrado. Cadastre-se.'); window.location = 'cadastro.php?opcao=a';</script>";
			exit;
		}
	}
	
	if ($categoria == "professor")
	{
		$Q = mysql_query("SELECT id, email, nome FROM pensaqui_professor WHERE email = '$email'");
		
		if (mysql_num_rows($Q) > 0)
		{
			$A = mysql_fetch_array($Q);
			require("envia_email.php");
			
			$nova_senha = substr(strtoupper(md5(date("dmYHis"))), 0, 6);
			
			$nova_senha64 = base64_encode($nova_senha);
			
			$U = mysql_query("UPDATE pensaqui_professor SET senha = '$nova_senha64' WHERE id = $A[id]");
			
			if ($U)
			{
				$texto = "Olá, $A[nome].<br /><br />Você solicitou uma nova senha para utilização do PensaQui. Sua nova senha é <b>$nova_senha</b>.<br /><br />PensaQui";
				$assunto = "Recuperação de senha";
				$destinatario = $email;
				
				$resultado = enviaEmail($texto, $destinatario, $assunto);
				
				if ($resultado)
					echo "<script>alert('Um e-mail foi enviado para o endereço cadastrado contendo uma nova senha.'); window.location = 'index.html';</script>";
				else
					echo "<script>alert('Ocorreu um erro no envio do e-mail de recuperação senha. Tente refazer o processo.'); window.location = 'esqueci.php';</script>";
			}
			else
			{
				echo "<script>alert('Ocorreu um problema para alteração da senha. Tente refazer o processo.'); window.location = 'esqueci.php';</script>";
			}
			
			exit;
		}
		else
		{
			echo "<script>alert('Este e-mail não foi encontrado. Cadastre-se.'); window.location = 'cadastro.php?opcao=p';</script>";
			exit;
		}
	}
}






?>
<!DOCTYPE html>
<html>
<head>
<title>ESQUECI A SENHA</title>
<meta charset="utf-8">

<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700"/>
<link rel="stylesheet" type="text/css" href="controle/scripts/css.css" >
	
<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script type="text/javascript" src="controle/scripts/painel.js"></script>
</head>

<body>
<div id="content">
<div id="nav">
<img src="controle/logo.png"/>
<p>Preencha seus dados ao lado.</p>
</div>


<div class="section">

	<h2>Esqueceu a senha?</h2>

	<form name='Fesqueci' id='Fesqueci' method='POST'>

		<p><b>Categoria: </b>
		<select name='categoria' id='categoria'>
			<option value='0'>Selecione</option>
			<option value='aluno'>Aluno</option>
			<option value='professor'>Professor</option>
		</select></p>

		<p><b>E-mail: </b>
		<input class="campos2" name="email" id="email"  type="text">
		<input class="buttoninterno" style='width: 400px;' type=submit value="RECEBER NOVA SENHA POR E-MAIL">

	</form>

	<script>
	$("#Fesqueci").submit(function(){
			if ($("#categoria").val() == 0)
			{
				alert("A categoria deve ser preenchida.");
				return false;
			}
			if ($("#email").val().length == 0)
			{
				alert("O e-mail deve ser preenchido.");
				$("#email").focus();
				return false;
			}
	});
	</script>

</div>

<div class="break"></div>
<div id="footer"></div>
</div>
</div>

</body>
</html>
