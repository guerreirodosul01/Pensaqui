



<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>PAINEL DE CONTROLE</title>

    <!-- Scripts -->
   
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="{{ asset('paineldecontrole/jquery.modal.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('paineldecontrole/painel.js') }}"></script>

    <!-- Fonts -->
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700"/>

    <!-- Styles -->
    <link href="{{ asset('paineldecontrole/jquery.modal.css') }}" type="text/css" rel="stylesheet" />
    <link href="{{ asset('paineldecontrole/jquery.modal.theme-xenon.css') }}" type="text/css" rel="stylesheet" />
    <link href="{{ asset('paineldecontrole/jquery.modal.theme-atlant.css') }}" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/controle/css.css') }}" >

    @yield('stylecss')
</head>
<body>
    <div id="content">
        <div id="nav">
            <img src="logo.png"/>
            <p>Bem-vindo(a), <?php //echo $_SESSION["pensaqui_nome"] ?>.</p>
            <ul>
                <li class="buttonmenu"><a href='aluno.php'>ALUNOS</a></li>
                <li class="buttonmenu"><a href='turma.php'>TURMAS</a></li>
                <li class="buttonmenu"><a href='professor.php'>PROFESSORES</a></li>
                <li class="buttonmenu"><a href='escola.php'>ESCOLAS</a></li>
                <li class="buttonmenu"><a href='usuarios.php'>USUÁRIOS</a></li>
                <li class="buttonmenu2"><a href='logout.php'>LOGOUT</a></li>
            </ul>
        </div>
            @yield('content')
        
    </div>

    @yield('javascript')
</body>
</html>
