<?php

use Faker\Generator as Faker;


$factory->define(App\Aluno::class, function (Faker $faker) {
    return [
        'turma' => $faker->text,
        'professor' => $faker->text,
        'nome' => $faker->name,
        'senha' => $faker->password,
        'email' => $faker->unique()->safeEmail
    ];
});
