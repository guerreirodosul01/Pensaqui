<?php

use Faker\Generator as Faker;

$factory->define(App\Professor::class, function (Faker $faker) {
    return [
        'escola'=> $faker->numberBetween(0,10000),
        'nome' => $faker->name,
        'senha' => $faker->password,
        'email' =>$faker->unique()->safeEmail,
        'login' => $faker->userName
    ];
});
