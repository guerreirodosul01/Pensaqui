<?php

use Faker\Generator as Faker;


$factory->define(App\Alunosresposta::class, function (Faker $faker) {
    return [
        'respostaa' => $faker->text,
        'respostab' => $faker->text,
        'respostac' => $faker->text,
        'respostad' => $faker->text
    ];
});
