<?php

use Faker\Generator as Faker;

$factory->define(App\Usuario::class, function (Faker $faker) {
    return [
        'nome' => $faker->name,
        'login' => $faker->userName,
        'senha' => $faker->password
    ];
});
