<?php

use Faker\Generator as Faker;

$factory->define(App\Turma::class, function (Faker $faker) {
    return [
        'nome' => $faker->name,
        'escola' => $faker->name,
        'professor' => $faker->name
    ];
});
