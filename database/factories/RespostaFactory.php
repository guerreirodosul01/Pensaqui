<?php

use Faker\Generator as Faker;

$factory->define(App\Resposta::class, function (Faker $faker) {
    return [
        'aluno' => $faker->numberBetween(1,10000),
        'tipo' => $faker->numberBetween(1,10000),
        'txt' => $faker->text
    ];
});
