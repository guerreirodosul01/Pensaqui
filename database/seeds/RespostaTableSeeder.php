<?php

use Illuminate\Database\Seeder;

class RespostaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Resposta::class, 10)->create();
    }
}
