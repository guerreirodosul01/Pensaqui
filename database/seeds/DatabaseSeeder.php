<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            AlunoTableSeeder::class,
            EscolaTableSeeder::class,
            ProfessorTableSeeder::class,
            RespostaTableSeeder::class,
            TurmaTableSeeder::class,
            UsuarioTableSeeder::class,
            AlunosrespostaTableSeeder::class

        ]);
    }
}
