<?php

use Illuminate\Database\Seeder;

class AlunosrespostaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Alunosresposta::class, 10)->create();
    }
}
