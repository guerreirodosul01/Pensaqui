<?php

use Illuminate\Database\Seeder;

class EscolaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Escola::class, 10)->create();
    }
}
