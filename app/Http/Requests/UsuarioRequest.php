<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsuarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()) {
            case "POST": // CRIAÇÃO DE UM NOVO REGISTRO
                return [

                    'nome' => 'required',
                    'senha' => 'required',

                    'login' => 'required|unique:usuarios'
                ];
                break;
            case "PUT": // ATUALIZAÇÃO DE UM REGISTRO EXISTENTE
                return [

                    'nome' => 'required',
                    'senha' => 'required',

                    'login' => 'required|unique:usuarios,email,'.$this->id
                ];
                break;
            default:break;
        }
    }

    public function messages()
    {
        return [
            'senha.required' => 'O campo Senha é obrigatório',
            'nome.required' => 'O campo Nome é obrigatório',
            'login.required' => 'O campo Login é obrigatório',
        ];
    }
}
