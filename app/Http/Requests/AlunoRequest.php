<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AlunoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()) {
            case "POST": // CRIAÇÃO DE UM NOVO REGISTRO
                return [
                    'turma' => 'required',
                    'nome' => 'required',
                    'senha' => 'required',
                    'email' => 'email|max:255|unique:alunos',
                    'professor' => 'required'
                ];
                break;
            case "PUT": // ATUALIZAÇÃO DE UM REGISTRO EXISTENTE
                return [
                    'turma' => 'required',
                    'nome' => 'required',
                    'senha' => 'required',
                    'email' => 'email|max:255|unique:alunos,email,'.$this->id,
                    'professor' => 'required'
                ];
                break;
            default:break;
        }
    }

    public function messages()
    {
        return [
            'turma.required' => 'O campo Turma é obrigatório',
            'senha.required' => 'O campo Senha é obrigatório',
            'nome.required' => 'O campo Nome é obrigatório',
            'email.email' => 'Informe um e-mail válido',
            'professor.required' => 'O campo Professor é obrigatório',
        ];
    }
}
