<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfessorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()) {
            case "POST": // CRIAÇÃO DE UM NOVO REGISTRO
                return [
                    'escola' => 'required',
                    'nome' => 'required',
                    'senha' => 'required',
                    'email' => 'email|max:255|unique:professors',
                    'login' => 'required||unique:professors'
                ];
                break;
            case "PUT": // ATUALIZAÇÃO DE UM REGISTRO EXISTENTE
                return [
                    'turma' => 'required',
                    'nome' => 'required',
                    'senha' => 'required',
                    'email' => 'email|max:255|unique:professors,email,'.$this->id,
                    'login' => 'required|unique:professors,email,'.$this->id
                ];
                break;
            default:break;
        }
    }

    public function messages()
    {
        return [
            'turma.required' => 'O campo Turma é obrigatório',
            'senha.required' => 'O campo Senha é obrigatório',
            'nome.required' => 'O campo Nome é obrigatório',
            'email.email' => 'Informe um e-mail válido',
            'login.required' => 'O campo Login é obrigatório',
        ];
    }
}
