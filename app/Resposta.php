<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resposta extends Model
{
    protected $fillable = array(
        "aluno","tipo","txt"
    );
}
