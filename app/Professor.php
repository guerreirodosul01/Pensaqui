<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Professor extends Model
{
    protected $fillable = array(
        "escola","nome","senha","email","login"
    );
}
