<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aluno extends Model
{
    protected $fillable = array(
        "turma","professor","nome","senha","email"
    );
}
